/**
 * 封装ajax函数
 * @param  param                   HTTP连接的方式，包括POST和GET两种方式
 * @param  param.url               发送请求的URL
 * @param  param.type              HTTP连接的方式，包括POST和GET两种方式
 * @param  param.dataType          预期服务器返回的数据类型('json', 'jsonp', 'xml', 'html', or 'text')
 * @param  param.async             是否为异步请求，true为异步的，false为同步的
 * @param  param.data              发送的参数，格式为对象类型
 * @param  param.timeout           请求超时时间
 * @param  param.success           请求发送并接收成功调用的回调函数
 * @param  param.error             请求发送并接收失败调用的回调函数
 * @param  param.complete          回调函数
 */

let ajax = function (param) {
  // 定义一些公共方法 -> 将对象序列化
  function params(obj) {
    let arr = [];
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        arr.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
      }
    }
    return arr.join('&');
  }

  // 定义一些公共方法 -> 网址检测
  function reRUL(url, data, type) {
    console.log(data);
    if (url) {
      console.log(data);
      url = url.replace(/(^[^#]+)(#.*$)/g, (S0, S1) => {
        return S1;
      });
      let _SEARCH = {};
      url = url.replace(/(^[^#]+)(\?[^#|\n]*)(?:.*$)/g, (S0, S1, S2) => {
        S2 = S2.replace(/^\?/, '').split('&');
        for (let i = 0, len = S2.length; i < len; i++) {
          let item = S2[i].split('=');
          _SEARCH[item[0]] = item[1];
        }
        return S1;
      });
      console.log(data);
      for (var key in data) {
        _SEARCH[key] = data[key];
      }
      console.log(_SEARCH);
      _SEARCH = params(_SEARCH);
      if (type === 'post') {
        return _SEARCH;
      } else {
        return url + (_SEARCH ? '?' + _SEARCH : '');
      }
    }
  }

  if (param) {
    let fun = function () {};
    let type = (param.type && param.type.toUpperCase()) || 'GET';
    let url = param.url || '';
    let async = param.async || true;
    let data = param.data || {};
    let contentType = param.contentType || 'application/x-www-form-urlencoded; charset=UTF-8';
    let dataType = param.dataType || '';
    let success = param.success || fun;
    let error = param.error || fun;
    let complete = param.complete || fun;
    let timeout = param.timeout || 7000;
    let timer = null;
    let accepts = {
      html: 'text/html',
      json: 'application/json, text/javascript',
      script: 'text/javascript, application/javascript, application/x-javascript, application/ecmascript, application/x-ecmascript',
      text: 'text/plain',
      xml: 'application/xml, text/xml'
    };

    let xhr = window.XMLHttpRequest ? new window.XMLHttpRequest() : new window.ActiveXObject('Microsoft.XMLHTTP');

    if (type === 'GET') {
      console.log(data);
      url = reRUL(url, data, 'get');
    }
    xhr.open(type, url, async);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    if (dataType) {
      xhr.setRequestHeader('Accept', accepts[dataType] + ', */*; q=0.01');
    }

    if (type === 'POST') {
      xhr.setRequestHeader('Content-Type', contentType);
      data = reRUL(url, data, 'post');
    } else {
      data = null;
    }
    xhr.send(data);

    xhr.onreadystatechange = () => {
      clearTimeout(timer);
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (dataType === 'xml') {
            success(xhr.responseXML);
            complete(xhr.responseXML);
          } else {
            let xhrData = xhr.responseText;
            if (dataType === 'json') {
              xhrData = JSON.parse(xhrData);
            }
            success(xhrData);
            complete(xhrData);
          }
        } else {
          error(xhr);
          complete(xhr);
        }
      }
    };
    timer = setTimeout(() => {
      xhr.abort();
      complete({
        msg: 'timeout'
      });
    }, timeout);
  }
};

module.exports = ajax;
