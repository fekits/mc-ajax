# MC-INVIEW
```$xslt
页面滚动时监听指定元素滚动到可视区域时播放动画
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[http://www.junbo.name/plugins/mc-inview](http://www.junbo.name/plugins/mc-inview)


#### 开始

下载项目: 

```npm
npm i @fekit/mc-inview
```

#### 参数
```$xslt
el      {String}   选择器
theme   {String}   插件配套的动画主题
offset  {Number}   元素触发动画时的偏移量 0-1
once    {Number}   是否仅播放一次动画
on:{
    view  {Function}  显示时触发
    none  {Function}  隐去时触发
}
```

#### 示例

```javascript
import McInview from '@fekit/mc-inview';

// 楼层埋点
new McInview({
  el: '.floor',
  on: {
    view(dom) {
      console.log(dom, '这个元素滚动到可视区了');
    }
  }
});
```

#### 版本

```$xslt
v1.0.6 
1. 新增3款主题
2. 修改HTML可自定义主题
```

```$xslt
v1.0.3 
1. 新增一款主题mc-inview@theme=ac.scss
```

```$xslt
v1.0.2 
1. 重构核心代码，优化性能
```
```$xslt
v1.0.1
1. 此版本精简了代码，取消了onshow和onhide，回调合并为一个then，可以通赤then回调的传参来判断是显示还是消失及其它一些操作。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
